package com.gitlab.trashcode.economy.message;


import com.gitlab.trashcode.api.messages.MessagesBase;

public enum Messages implements MessagesBase {

    NO_PERMISSION("&cНедостаточно прав"),
    INVALID_NUMBER("&cАргумент #{number} должен быть числом"),
    PLAYER_NOT_FOUND("&cИгрок не найден"),
    CURRENCY_NOT_FOUND("&cВалюта не найдена"),
    USAGE(
            "&7========[&eEconomy&7]========",
            "&7- /economy give <user> <currency> <amount> - выдать деньги игроку",
            "&7- /economy take <user> <currency> <amount> - забрать деньги у игрока",
            "&7- /economy reset <user> <currency*> - обнулить счет",
            "&7- /economy transfer <user> <currencyFrom> <currencyTo> <amount> - перевести из одной валюты в другую игроку",
            "&7- /balance <user*> - просмотреть баланс (свой/игрока)",
            "&7- /transfer <currencyFrom> <currencyTo> <amount> - перевести из одной валюты в другую",
            "&7- /pay <user> <currency> <amount> - отправить деньги игроку",
            "&7========================="
    ),
    BALANCE_FORMAT_HEADER(
            "&7========[&bBalance&7]========",
            "&7Balance of player {user}:"
    ),
    BALANCE_FORMAT_CURRENCY_LINE(
            "&7- {currency}: {balance}"
    ),
    BALANCE_FORMAT_FOOTER("&7========================="),
    YOU_SUCCESSFULLY_GIVE_MONEY("&aВы начислили игроку {target} {amount}{currency}"),
    YOU_GET_MONEY("&a{sender} начислил вам {amount}{currency}"),
    YOU_SUCCESSFULLY_TAKE_MONEY("&aВы сняли со счета игрока {target} {amount}{currency}"),
    PLAYER_TAKE_YOUR_MONEY("&cИгрок {sender} снял с вашего счета {amount}{currency}"),
    PLAYER_NOT_HAVE_MONEY("&cИгрок {target} не имеет {amount}{currency} на счете"),
    YOU_SUCCESSFULLY_TRANSFER_MONEY_FOR_PLAYER("&aВы перевели {amount} из {currencyFrom} в {currencyTo} у игрока {target}"),
    PLAYER_TRANSFER_MONEY_FOR_YOU("&eИгрок {sender} перевел {amount} из {currencyFrom} в {currencyTo} на вашем счете"),
    YOU_SUCCESSFULLY_RESET_BALANCE_FOR_PLAYER("&aВы обнулили счет игроку {target}"),
    PLAYER_RESET_BALANCE_FOR_YOU("&cИгрок {sender} обнулил вам счет"),
    YOU_NOT_HAVE_MONEY("&cУ вас нет {amount}{currency} на счете"),
    YOU_SUCCESSFULLY_TRANSFER_MONEY("&aВы перевели {amount} из {currencyFrom} в {currencyTo} на своем счете"),
    YOU_SEND_MONEY_TO_PLAYER("&aВы отправили {amount}{currency} игроку {target}"),
    PLAYER_SEND_MONEY_TO_YOU("&aИгрок {sender} перевел вам {amount}{currency}");

    private String[] message;

    Messages(String... message) {
        this.message = message;
    }

    @Override
    public String getPath() {
        return name().toLowerCase().replace("__", ".").replace("_", "-");
    }

    @Override
    public String[] getMessage() {
        return message;
    }
}
