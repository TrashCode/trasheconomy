package com.gitlab.trashcode.economy.vault;

import com.gitlab.trashcode.economy.api.EconomyCurrencies;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;

public class Economy_TrashEconomy implements Economy {

    private EconomyCurrencies economyCurrencies;
    private EconomyUserManager userManager;

    public Economy_TrashEconomy(EconomyCurrencies economyCurrencies, EconomyUserManager userManager) {
        this.economyCurrencies = economyCurrencies;
        this.userManager = userManager;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "TrashEconomy";
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return 0;
    }

    @Override
    public String format(double v) {
        throw new UnsupportedOperationException("The method \"format\" is unsupported");
    }

    @Override
    public String currencyNamePlural() {
        return economyCurrencies.getPrimaryCurrency().getDisplayName();
    }

    @Override
    public String currencyNameSingular() {
        return economyCurrencies.getPrimaryCurrency().getDisplayName();
    }

    @Override
    public boolean hasAccount(String s) {
        return userManager.getUser(s) != null;
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer) {
        return userManager.getUser(offlinePlayer.getName()) != null;
    }

    @Override
    public boolean hasAccount(String s, String s1) {
        return hasAccount(s);
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
        return hasAccount(offlinePlayer);
    }

    @Override
    public double getBalance(String s) {
        return userManager.getUser(s).getBalance(economyCurrencies.getPrimaryCurrency());
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer) {
        return getBalance(offlinePlayer.getName());
    }

    @Override
    public double getBalance(String s, String s1) {
        return getBalance(s);
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer, String s) {
        return getBalance(offlinePlayer);
    }

    @Override
    public boolean has(String s, double v) {
        return userManager.getUser(s).has(economyCurrencies.getPrimaryCurrency(), v);
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, double v) {
        return has(offlinePlayer.getName(), v);
    }

    @Override
    public boolean has(String s, String s1, double v) {
        return has(s, v);
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
        return has(offlinePlayer, v);
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, double v) {
        EconomyUser user = userManager.getUser(s);
        user.withdraw(economyCurrencies.getPrimaryCurrency(), v);
        return new EconomyResponse(v, user.getBalance(economyCurrencies.getPrimaryCurrency()), EconomyResponse.ResponseType.SUCCESS, "Account is not have money");
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
        return withdrawPlayer(offlinePlayer.getName(), v);
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, String s1, double v) {
        return withdrawPlayer(s, v);
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return withdrawPlayer(offlinePlayer, v);
    }

    @Override
    public EconomyResponse depositPlayer(String s, double v) {
        EconomyUser user = userManager.getUser(s);
        user.deposit(economyCurrencies.getPrimaryCurrency(), v);
        return new EconomyResponse(v, user.getBalance(economyCurrencies.getPrimaryCurrency()), EconomyResponse.ResponseType.SUCCESS, "Account is not have money");
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
        return depositPlayer(offlinePlayer.getName(), v);
    }

    @Override
    public EconomyResponse depositPlayer(String s, String s1, double v) {
        return depositPlayer(s, v);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return depositPlayer(offlinePlayer, v);
    }

    @Override
    public EconomyResponse createBank(String s, String s1) {
        throw new UnsupportedOperationException("The method \"createBank\" is unsupported");
    }

    @Override
    public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
        return createBank(s, offlinePlayer.getName());
    }

    @Override
    public EconomyResponse deleteBank(String s) {
        throw new UnsupportedOperationException("The method \"deleteBank\" is unsupported");
    }

    @Override
    public EconomyResponse bankBalance(String s) {
        throw new UnsupportedOperationException("The method \"bankBalance\" is unsupported");
    }

    @Override
    public EconomyResponse bankHas(String s, double v) {
        throw new UnsupportedOperationException("The method \"bankHas\" is unsupported");
    }

    @Override
    public EconomyResponse bankWithdraw(String s, double v) {
        throw new UnsupportedOperationException("The method \"bankWithdraw\" is unsupported");
    }

    @Override
    public EconomyResponse bankDeposit(String s, double v) {
        throw new UnsupportedOperationException("The method \"bankDeposit\" is unsupported");
    }

    @Override
    public EconomyResponse isBankOwner(String s, String s1) {
        throw new UnsupportedOperationException("The method \"isBankOwner\" is unsupported");
    }

    @Override
    public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
        return isBankOwner(s, offlinePlayer.getName());
    }

    @Override
    public EconomyResponse isBankMember(String s, String s1) {
        throw new UnsupportedOperationException("The method \"isBankMember\" is unsupported");
    }

    @Override
    public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
        return isBankMember(s, offlinePlayer.getName());
    }

    @Override
    public List<String> getBanks() {
        return new ArrayList<>();
    }

    @Override
    public boolean createPlayerAccount(String s) {
        try {
            userManager.addUser(s);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
        return createPlayerAccount(offlinePlayer.getName());
    }

    @Override
    public boolean createPlayerAccount(String s, String s1) {
        return createPlayerAccount(s);
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
        return createPlayerAccount(offlinePlayer);
    }

}
