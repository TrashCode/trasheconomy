package com.gitlab.trashcode.economy.papi;

import com.gitlab.trashcode.economy.api.EconomyCurrency;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;
import org.bukkit.OfflinePlayer;

public class TrashEconomyHook extends PlaceholderHook {

    private EconomyUserManager userManager;

    public TrashEconomyHook(EconomyUserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public String onRequest(OfflinePlayer p, String params) {
        EconomyUser user = userManager.getUser(p.getName());
        for(EconomyCurrency currency : user.getAvailableCurrencies()) {
            if(params.equals("balance_" + currency.getName())) {
                return String.valueOf(user.getBalance(currency));
            }
        }
        return "-";
    }

    public void hook() {
        PlaceholderAPI.registerPlaceholderHook("trasheconomy", this);
    }

}
