package com.gitlab.trashcode.economy.command;

import com.gitlab.trashcode.api.command.TrashCommandExecutor;
import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.economy.api.EconomyCurrency;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import com.gitlab.trashcode.economy.message.Messages;
import org.bukkit.command.CommandSender;

public class BalanceCommandExecutor implements TrashCommandExecutor {

    private EconomyUserManager userManager;
    private MessageSender messageSender;

    public BalanceCommandExecutor(EconomyUserManager userManager, MessageSender messageSender) {
        this.userManager = userManager;
        this.messageSender = messageSender;
    }

    @Override
    public void execute(CommandSender commandSender, String s, String[] strings) {
        EconomyUser user = userManager.getUser(commandSender.getName());
        if(strings.length > 0) {
            user = userManager.getUser(strings[0]);
            if(!commandSender.hasPermission("trasheconomy.balance.other")) {
                messageSender.get(Messages.NO_PERMISSION).send(commandSender);
                return;
            }
            if(user == null) {
                messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
                return;
            }
        }
        messageSender.get(Messages.BALANCE_FORMAT_HEADER, "user", user.getName()).send(commandSender);
        for (EconomyCurrency currency : user.getAvailableCurrencies()) {
            messageSender.get(Messages.BALANCE_FORMAT_CURRENCY_LINE, "currency", currency.getDisplayName(), "balance", user.getBalance(currency)).send(commandSender);
        }
        messageSender.get(Messages.BALANCE_FORMAT_FOOTER).send(commandSender);
    }

}
