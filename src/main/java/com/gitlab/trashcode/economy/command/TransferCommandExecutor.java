package com.gitlab.trashcode.economy.command;

import com.gitlab.trashcode.api.command.TrashCommandExecutor;
import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.economy.api.EconomyCurrencies;
import com.gitlab.trashcode.economy.api.EconomyCurrency;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import com.gitlab.trashcode.economy.message.Messages;
import org.bukkit.command.CommandSender;

public class TransferCommandExecutor implements TrashCommandExecutor {

    private MessageSender messageSender;
    private EconomyUserManager userManager;
    private EconomyCurrencies economyCurrencies;

    public TransferCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
        this.messageSender = messageSender;
        this.userManager = userManager;
        this.economyCurrencies = economyCurrencies;
    }

    @Override
    public void execute(CommandSender commandSender, String s, String[] args) {
        if(args.length < 3) {
            messageSender.get(Messages.USAGE).send(commandSender);
            return;
        }
        EconomyUser user = userManager.getUser(commandSender.getName());
        EconomyCurrency fromCurrency = economyCurrencies.get(args[0]);
        double amount = Double.parseDouble(args[2]);
        if(!user.has(fromCurrency, amount)) {
            messageSender.get(Messages.YOU_NOT_HAVE_MONEY, "amount", amount, "currency", fromCurrency.getDisplayName()).send(commandSender);
            return;
        }
        EconomyCurrency toCurrency = economyCurrencies.get(args[1]);
        user.transfer(fromCurrency, toCurrency, amount);
        messageSender.get(Messages.YOU_SUCCESSFULLY_TRANSFER_MONEY, "amount", amount, "currencyFrom", fromCurrency.getDisplayName(), "currencyTo", toCurrency.getDisplayName()).send(commandSender);
    }

}
