package com.gitlab.trashcode.economy.command;

import com.gitlab.trashcode.api.command.TrashCommandExecutor;
import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.economy.api.EconomyCurrencies;
import com.gitlab.trashcode.economy.api.EconomyCurrency;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import com.gitlab.trashcode.economy.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PayCommandExecutor implements TrashCommandExecutor {

    private MessageSender messageSender;
    private EconomyUserManager userManager;
    private EconomyCurrencies economyCurrencies;

    public PayCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
        this.messageSender = messageSender;
        this.userManager = userManager;
        this.economyCurrencies = economyCurrencies;
    }

    @Override
    public void execute(CommandSender commandSender, String s, String[] args) {
        if(args.length < 3) {
            messageSender.get(Messages.USAGE).send(commandSender);
            return;
        }
        EconomyUser user = userManager.getUser(commandSender.getName());
        EconomyUser targetUser = userManager.getUser(args[0]);
        if(targetUser == null) {
            messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
            return;
        }
        EconomyCurrency currency = economyCurrencies.get(args[1]);
        double amount = Double.parseDouble(args[2]);
        if(!user.has(currency, amount)) {
            messageSender.get(Messages.YOU_NOT_HAVE_MONEY, "amount", amount, "currency", currency).send(commandSender);
            return;
        }
        user.send(user, currency, amount);
        messageSender.get(Messages.YOU_SEND_MONEY_TO_PLAYER, "amount", amount, "currency", currency.getDisplayName(), "target", targetUser.getName()).send(commandSender);
        Player target = Bukkit.getPlayer(targetUser.getName());
        if(target != null) messageSender.get(Messages.PLAYER_SEND_MONEY_TO_YOU, "sender", user.getName(), "amount", amount, "currency", currency.getDisplayName()).send(target);
    }

}
