package com.gitlab.trashcode.economy.command;

import com.gitlab.trashcode.api.command.TrashCommandExecutor;
import com.gitlab.trashcode.api.command.TrashSubCommandExecutor;
import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.economy.api.EconomyCurrencies;
import com.gitlab.trashcode.economy.api.EconomyCurrency;
import com.gitlab.trashcode.economy.api.EconomyUser;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import com.gitlab.trashcode.economy.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EconomyCommandExecutor implements TrashCommandExecutor {

    private MessageSender messageSender;

    public EconomyCommandExecutor(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @Override
    public void execute(CommandSender commandSender, String s, String[] strings) {
        messageSender.get(Messages.USAGE).send(commandSender);
    }

    private static abstract class EconomySubCommandExecutor implements TrashSubCommandExecutor {

        MessageSender messageSender;
        EconomyUserManager userManager;
        EconomyCurrencies economyCurrencies;

        EconomySubCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
            this.messageSender = messageSender;
            this.userManager = userManager;
            this.economyCurrencies = economyCurrencies;
        }

    }

    public static class GiveSubCommandExecutor extends EconomySubCommandExecutor {

        public GiveSubCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
            super(messageSender, userManager, economyCurrencies);
        }

        @Override
        public void execute(CommandSender commandSender, String s, String s1, String[] args) {
            if(args.length < 3) {
                messageSender.get(Messages.USAGE).send(commandSender);
                return;
            }
            EconomyUser user = userManager.getUser(args[0]);
            if(user == null) {
                messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
                return;
            }
            EconomyCurrency currency = economyCurrencies.get(args[1]);
            double amount = Double.parseDouble(args[2]);
            user.deposit(currency, amount);
            messageSender.get(Messages.YOU_SUCCESSFULLY_GIVE_MONEY, "target", user.getName(), "amount", amount, "currency", currency.getDisplayName()).send(commandSender);
            Player target = Bukkit.getPlayer(user.getName());
            if(target != null) messageSender.get(Messages.YOU_GET_MONEY, "sender", commandSender.getName(), "amount", amount, "currency", currency.getDisplayName()).send(target);
        }

    }

    public static class TakeSubCommandExecutor extends EconomySubCommandExecutor {

        public TakeSubCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
            super(messageSender, userManager, economyCurrencies);
        }

        @Override
        public void execute(CommandSender commandSender, String s, String s1, String[] args) {
            if(args.length < 3) {
                messageSender.get(Messages.USAGE).send(commandSender);
                return;
            }
            EconomyUser user = userManager.getUser(args[0]);
            if(user == null) {
                messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
                return;
            }
            EconomyCurrency currency = economyCurrencies.get(args[1]);
            double amount = Double.parseDouble(args[2]);
            if(!user.has(currency, amount)) {
                messageSender.get(Messages.PLAYER_NOT_HAVE_MONEY, "target", user.getName(), "amount", amount, "currency", currency.getDisplayName()).send(commandSender);
                return;
            }
            user.withdraw(currency, amount);
            messageSender.get(Messages.YOU_SUCCESSFULLY_TAKE_MONEY, "target", user.getName(), "amount", amount, "currency", currency.getDisplayName()).send(commandSender);
            Player target = Bukkit.getPlayer(user.getName());
            if(target != null) messageSender.get(Messages.PLAYER_TAKE_YOUR_MONEY, "sender", commandSender.getName(), "amount", amount, "currency", currency.getDisplayName()).send(target);
        }

    }

    public static class TransferSubCommandExecutor extends EconomySubCommandExecutor {

        public TransferSubCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
            super(messageSender, userManager, economyCurrencies);
        }

        @Override
        public void execute(CommandSender commandSender, String s, String s1, String[] args) {
            if(args.length < 4) {
                messageSender.get(Messages.USAGE).send(commandSender);
                return;
            }
            EconomyUser user = userManager.getUser(args[0]);
            if(user == null) {
                messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
                return;
            }
            EconomyCurrency fromCurrency = economyCurrencies.get(args[1]);
            double amount = Double.parseDouble(args[1]);
            if(!user.has(fromCurrency, amount)) {
                messageSender.get(Messages.PLAYER_NOT_HAVE_MONEY, "target", user.getName(), "amount", amount, "currency", fromCurrency.getDisplayName()).send(commandSender);
                return;
            }
            EconomyCurrency toCurrency = economyCurrencies.get(args[2]);
            user.transfer(fromCurrency, toCurrency, amount);
            messageSender.get(Messages.YOU_SUCCESSFULLY_TRANSFER_MONEY_FOR_PLAYER, "amount", amount, "currencyFrom", fromCurrency.getDisplayName(), "currencyTo", toCurrency.getDisplayName(), "target", user.getName()).send(commandSender);
            Player target = Bukkit.getPlayer(user.getName());
            if(target != null) messageSender.get(Messages.PLAYER_TRANSFER_MONEY_FOR_YOU, "sender", commandSender.getName(), "amount", amount, "currencyFrom", fromCurrency.getDisplayName(), "currencyTo", toCurrency.getDisplayName()).send(target);
        }
    }

    public static class ResetSubCommandExecutor extends EconomySubCommandExecutor {

        public ResetSubCommandExecutor(MessageSender messageSender, EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
            super(messageSender, userManager, economyCurrencies);
        }

        @Override
        public void execute(CommandSender commandSender, String s, String s1, String[] args) {
            if(args.length < 1) {
                messageSender.get(Messages.USAGE).send(commandSender);
                return;
            }
            EconomyUser user = userManager.getUser(args[0]);
            if(user == null) {
                messageSender.get(Messages.PLAYER_NOT_FOUND).send(commandSender);
                return;
            }
            if(args.length > 1) {
                user.reset(economyCurrencies.get(args[1]));
            } else {
                user.reset();
            }
            messageSender.get(Messages.YOU_SUCCESSFULLY_RESET_BALANCE_FOR_PLAYER, "target", user.getName()).send(commandSender);
            Player target = Bukkit.getPlayer(user.getName());
            if(target != null) messageSender.get(Messages.PLAYER_RESET_BALANCE_FOR_YOU, "sender", commandSender.getName()).send(target);
        }

    }

}
