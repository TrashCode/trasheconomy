package com.gitlab.trashcode.economy.listener;

import com.gitlab.trashcode.economy.api.EconomyUserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

    private EconomyUserManager userManager;

    public PlayerListener(EconomyUserManager userManager) {
        this.userManager = userManager;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if(userManager.getUser(event.getPlayer().getName()) == null) userManager.addUser(event.getPlayer().getName());
    }

}
