package com.gitlab.trashcode.economy.api;

public interface EconomyCurrency {

    String getName();
    String getDisplayName();
    double getCurrencyCourse(EconomyCurrency currency);
    boolean isPrimary();

}
