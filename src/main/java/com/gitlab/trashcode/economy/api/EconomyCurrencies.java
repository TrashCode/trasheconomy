package com.gitlab.trashcode.economy.api;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EconomyCurrencies {

    private FileConfiguration config;

    private EconomyCurrencies(FileConfiguration config) {
        this.config = config;
    }

    public static EconomyCurrencies fromConfig(FileConfiguration config) {
        return new EconomyCurrencies(config);
    }

    public EconomyCurrency get(String currencyName) {
        ConfigurationSection section = config.getConfigurationSection("settings.currencies." + currencyName.toLowerCase());
        if(section == null) throw new CurrencyNotFoundException("Currency " + currencyName + " not found");
        return new EconomyCurrencyImpl(currencyName.toLowerCase(), section.getString("displayName"), section.getConfigurationSection("course").getKeys(false).stream().collect(Collectors.toMap(Function.identity(), str -> section.getDouble("course." + str))), section.getBoolean("isPrimary", false));
    }

    public EconomyCurrency getPrimaryCurrency() {
        return config.getConfigurationSection("settings.currencies").getKeys(false).stream().map(this::get).filter(EconomyCurrency::isPrimary).findFirst().orElse(null);
    }

    private class EconomyCurrencyImpl implements EconomyCurrency {

        private String name;
        private String displayName;
        private Map<String, Double> currencyCourses;
        private boolean isPrimary;

        EconomyCurrencyImpl(String name, String displayName, Map<String, Double> currencyCourses, boolean isPrimary) {
            this.name = name;
            this.displayName = displayName;
            this.currencyCourses = currencyCourses;
            this.isPrimary = isPrimary;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getDisplayName() {
            return displayName;
        }

        @Override
        public double getCurrencyCourse(EconomyCurrency currency) {
            return currencyCourses.getOrDefault(currency.getName(), 0D);
        }

        @Override
        public boolean isPrimary() {
            return isPrimary;
        }
    }

}
