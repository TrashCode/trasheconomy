package com.gitlab.trashcode.economy.api;

public class TrashEconomyApi {

    private EconomyUserManager userManager;
    private EconomyCurrencies economyCurrencies;

    public TrashEconomyApi(EconomyUserManager userManager, EconomyCurrencies economyCurrencies) {
        this.userManager = userManager;
        this.economyCurrencies = economyCurrencies;
    }

    public EconomyUserManager getUserManager() {
        return userManager;
    }

    public EconomyCurrencies getEconomyCurrencies() {
        return economyCurrencies;
    }

}
