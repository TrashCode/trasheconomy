package com.gitlab.trashcode.economy.api;

import com.gitlab.trashcode.api.query.QueryFactory;
import com.gitlab.trashcode.api.query.util.ResultTable;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.*;
import java.util.stream.Collectors;

public class EconomyUserManager {

    private List<EconomyUser> users;
    private QueryFactory queryFactory;
    private FileConfiguration config;

    public EconomyUserManager(QueryFactory queryFactory, FileConfiguration config) {
        this.config = config;
        this.queryFactory = queryFactory;
        this.users = new ArrayList<>();
        queryFactory.createQuery("SELECT * FROM `ECONOMYUSERS`").executeWithResult().getRows()
                .forEach(row -> {
                    Map<String, Double> moneysMap = new HashMap<>();
                    for (ResultTable.Row secondRow : queryFactory.createQuery("SELECT * FROM `ECONOMYUSERS` WHERE `NAME` = '{0}'", row.get("NAME").asString()).executeWithResult().getRows()) {
                        moneysMap.put(secondRow.get("CURRENCY").asString(), secondRow.get("VALUE").asDouble());
                    }
                    EconomyUser user = new EconomyUserImpl(row.get("NAME").asString(), moneysMap);
                    users.add(user);
                });
    }

    public EconomyUser getUser(String userName) {
        return users.stream().filter(user -> user.getName().equalsIgnoreCase(userName)).findFirst().orElse(null);
    }

    public void addUser(String userName) {
        Map<String, Double> balanceMap = new HashMap<>();
        balanceMap.put(EconomyCurrencies.fromConfig(config).getPrimaryCurrency().getName(), 0D);
        users.add(new EconomyUserImpl(userName, balanceMap));
    }

    public List<EconomyUser> getUsers() {
        return users;
    }

    public List<EconomyUser> getSortedUsers(EconomyCurrency sortedBy) {
        return users.stream().sorted(Comparator.comparingDouble(user -> user.getBalance(sortedBy))).collect(Collectors.toList());
    }

    public void save() {
        queryFactory.createQuery("DELETE FROM `ECONOMYUSERS`").execute();
        for(EconomyUser user : users) {
            for(EconomyCurrency currency : user.getAvailableCurrencies()) {
                queryFactory.createQuery("INSERT INTO `ECONOMYUSERS` (`NAME`, `CURRENCY`, `VALUE`) VALUES('{0}', '{1}', '{2}')", user.getName(), currency.getName(), user.getBalance(currency)).execute();
            }
        }
    }

    private class EconomyUserImpl implements EconomyUser {

        private String name;
        private Map<String, Double> balance;

        EconomyUserImpl(String name, Map<String, Double> balance) {
            this.name = name;
            this.balance = balance;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public double getBalance(EconomyCurrency currency) {
            return balance.getOrDefault(currency.getName(), 0D);
        }

        @Override
        public List<EconomyCurrency> getAvailableCurrencies() {
            EconomyCurrencies economyCurrencies = EconomyCurrencies.fromConfig(config);
            return balance.keySet().stream().map(economyCurrencies::get).collect(Collectors.toList());
        }

        @Override
        public void transfer(EconomyCurrency from, EconomyCurrency to, double amount) {
            balance.put(to.getName(), amount*from.getCurrencyCourse(to) + balance.getOrDefault(to.getName(), 0D));
            balance.put(from.getName(), balance.getOrDefault(from.getName(), 0D) - amount);
        }

        @Override
        public void deposit(EconomyCurrency currency, double amount) {
            balance.put(currency.getName(), balance.getOrDefault(currency.getName(), 0D) + amount);
        }

        @Override
        public void withdraw(EconomyCurrency currency, double amount) {
            balance.put(currency.getName(), balance.getOrDefault(currency.getName(), 0D) - amount);
        }

        @Override
        public void set(EconomyCurrency currency, double value) {
            balance.put(currency.getName(), value);
        }

        @Override
        public void send(EconomyUser user, EconomyCurrency currency, double amount) {
            user.deposit(currency, amount);
            withdraw(currency, amount);
        }

        @Override
        public boolean has(EconomyCurrency currency, double amount) {
            return balance.getOrDefault(currency.getName(), 0D) >= amount;
        }

        @Override
        public void reset(EconomyCurrency currency) {
            balance.remove(currency.getName());
            if(balance.size() == 0) {
                balance.put(EconomyCurrencies.fromConfig(config).getPrimaryCurrency().getName(), 0D);
            }
        }

        @Override
        public void reset() {
            balance.clear();
            balance.put(EconomyCurrencies.fromConfig(config).getPrimaryCurrency().getName(), 0D);
        }
    }

}
