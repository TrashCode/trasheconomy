package com.gitlab.trashcode.economy.api;

import java.util.List;

public interface EconomyUser {

    String getName();
    double getBalance(EconomyCurrency currency);
    List<EconomyCurrency> getAvailableCurrencies();
    void transfer(EconomyCurrency from, EconomyCurrency to, double amount);
    void deposit(EconomyCurrency currency, double amount);
    void withdraw(EconomyCurrency currency, double amount);
    void set(EconomyCurrency currency, double value);
    void send(EconomyUser user, EconomyCurrency currency, double amount);
    boolean has(EconomyCurrency currency, double value);
    void reset(EconomyCurrency currency);
    void reset();

}
