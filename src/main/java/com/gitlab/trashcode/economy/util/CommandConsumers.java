package com.gitlab.trashcode.economy.util;

import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.api.messages.MessagesBase;
import org.bukkit.command.CommandSender;

import java.util.function.Consumer;

public class CommandConsumers {

    public static Consumer<CommandSender> message(MessageSender messageSender, MessagesBase messagesBase, Object... replaces) {
        return commandSender -> messageSender.get(messagesBase, replaces).send(commandSender);
    }

}
