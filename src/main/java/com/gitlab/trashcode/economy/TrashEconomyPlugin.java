package com.gitlab.trashcode.economy;

import com.gitlab.trashcode.api.command.TrashCommand;
import com.gitlab.trashcode.api.command.TrashCommands;
import com.gitlab.trashcode.api.command.TrashSubCommand;
import com.gitlab.trashcode.api.command.util.TrashCommandPreconditions;
import com.gitlab.trashcode.api.connection.AbstractConnection;
import com.gitlab.trashcode.api.connection.H2Connection;
import com.gitlab.trashcode.api.connection.MySQLConnection;
import com.gitlab.trashcode.api.messages.MessageSender;
import com.gitlab.trashcode.api.messages.MessagesFactory;
import com.gitlab.trashcode.api.messages.MessagesFactoryBuilder;
import com.gitlab.trashcode.api.query.QueryFactory;
import com.gitlab.trashcode.economy.api.CurrencyNotFoundException;
import com.gitlab.trashcode.economy.api.EconomyCurrencies;
import com.gitlab.trashcode.economy.api.EconomyUserManager;
import com.gitlab.trashcode.economy.api.TrashEconomyApi;
import com.gitlab.trashcode.economy.command.BalanceCommandExecutor;
import com.gitlab.trashcode.economy.command.EconomyCommandExecutor;
import com.gitlab.trashcode.economy.command.PayCommandExecutor;
import com.gitlab.trashcode.economy.command.TransferCommandExecutor;
import com.gitlab.trashcode.economy.listener.PlayerListener;
import com.gitlab.trashcode.economy.message.Messages;
import com.gitlab.trashcode.economy.papi.TrashEconomyHook;
import com.gitlab.trashcode.economy.util.CommandConsumers;
import com.gitlab.trashcode.economy.vault.Economy_TrashEconomy;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Collections;

public class TrashEconomyPlugin extends JavaPlugin {

    private static TrashEconomyApi economyApi;

    private AbstractConnection abstractConnection;
    private EconomyUserManager userManager;
    private EconomyCurrencies economyCurrencies;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.economyCurrencies = EconomyCurrencies.fromConfig(getConfig());
        ConfigurationSection section = getConfig().getConfigurationSection("settings.mysql");
        this.abstractConnection = section == null || !section.getBoolean("enable") ? new H2Connection(new File(getDataFolder(), "data/mybase")) : new MySQLConnection(section.getString("host"), section.getString("database"), section.getString("user"), section.getString("password"));
        QueryFactory queryFactory = QueryFactory.connect(abstractConnection.getDataSource());
        queryFactory.createQuery("CREATE TABLE IF NOT EXISTS `ECONOMYUSERS` (`NAME` VARCHAR(16), `CURRENCY` VARCHAR, `VALUE` DOUBLE)").execute();
        MessagesFactory messagesFactory = MessagesFactoryBuilder.defaultBuilder(this).register(Messages.class).build();
        MessageSender messageSender = messagesFactory.getMessageSender();
        this.userManager = new EconomyUserManager(QueryFactory.connect(abstractConnection.getDataSource()), getConfig());
        economyApi = new TrashEconomyApi(userManager, economyCurrencies);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(userManager), this);
        TrashCommand economyCommand = TrashCommand.builder("economy")
                .precondition(TrashCommandPreconditions.exception(NumberFormatException.class).orElse(CommandConsumers.message(messageSender, Messages.INVALID_NUMBER, "number", 5)))
                .precondition(TrashCommandPreconditions.exception(CurrencyNotFoundException.class).orElse(CommandConsumers.message(messageSender, Messages.CURRENCY_NOT_FOUND)))
                .withSubCommand(TrashSubCommand.builder("give")
                        .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.give").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                        .executor(new EconomyCommandExecutor.GiveSubCommandExecutor(messageSender, userManager, economyCurrencies)).build())
                .withSubCommand(TrashSubCommand.builder("take")
                        .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.take").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                        .executor(new EconomyCommandExecutor.TakeSubCommandExecutor(messageSender, userManager, economyCurrencies)).build())
                .withSubCommand(TrashSubCommand.builder("transfer")
                        .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.transfer.other").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                        .executor(new EconomyCommandExecutor.TransferSubCommandExecutor(messageSender, userManager, economyCurrencies)).build())
                .withSubCommand(TrashSubCommand.builder("reset")
                        .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.reset"))
                        .executor(new EconomyCommandExecutor.ResetSubCommandExecutor(messageSender, userManager, economyCurrencies)).build())
                .executor(new EconomyCommandExecutor(messageSender)).build();
        TrashCommand balanceCommand = TrashCommand.builder("balance")
                .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.balance").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                .aliases(Collections.singletonList("bal"))
                .executor(new BalanceCommandExecutor(userManager, messageSender)).build();
        TrashCommand transferCommand = TrashCommand.builder("transfer")
                .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.transfer").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                .precondition(TrashCommandPreconditions.exception(NumberFormatException.class).orElse(CommandConsumers.message(messageSender, Messages.INVALID_NUMBER, "number", 4)))
                .precondition(TrashCommandPreconditions.exception(CurrencyNotFoundException.class).orElse(CommandConsumers.message(messageSender, Messages.CURRENCY_NOT_FOUND)))
                .executor(new TransferCommandExecutor(messageSender, userManager, economyCurrencies)).build();
        TrashCommand payCommand = TrashCommand.builder("pay")
                .precondition(TrashCommandPreconditions.bukkitPermission("trasheconomy.pay").orElse(CommandConsumers.message(messageSender, Messages.NO_PERMISSION)))
                .precondition(TrashCommandPreconditions.exception(NumberFormatException.class).orElse(CommandConsumers.message(messageSender, Messages.INVALID_NUMBER, "number", 4)))
                .precondition(TrashCommandPreconditions.exception(CurrencyNotFoundException.class).orElse(CommandConsumers.message(messageSender, Messages.CURRENCY_NOT_FOUND)))
                .executor(new PayCommandExecutor(messageSender, userManager, economyCurrencies)).build();
        TrashCommands.getCommandManager().register(economyCommand, balanceCommand, transferCommand, payCommand);
        if(Bukkit.getPluginManager().getPlugin("Vault") != null)
            Bukkit.getServicesManager().register(Economy.class, new Economy_TrashEconomy(economyCurrencies, userManager), this, ServicePriority.Normal);
        if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
            new TrashEconomyHook(userManager).hook();
    }

    @Override
    public void onDisable() {
        userManager.save();
        abstractConnection.close();
    }

    public static TrashEconomyApi getEconomyApi() {
        return economyApi;
    }

    public EconomyUserManager getUserManager() {
        return userManager;
    }

    public EconomyCurrencies getEconomyCurrencies() {
        return economyCurrencies;
    }

}
